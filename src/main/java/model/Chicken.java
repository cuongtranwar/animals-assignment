package model;

import constant.AnimalSingEnum;

public class Chicken extends Bird {
    @Override
    public void sing(AnimalSingEnum animalSingEnum) {
        System.out.println(animalSingEnum);
    }

    @Override
    public void fly() {
        System.out.println("I can not fly");
    }

}
