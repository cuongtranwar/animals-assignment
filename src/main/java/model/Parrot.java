package model;

import constant.ParrotSingEnum;

public class Parrot extends Bird {
    @Override
    public void swim() {
        System.out.println("I can not swim");
    }

    public void secondvoice(ParrotSingEnum parrotSingEnum) {
        System.out.println(parrotSingEnum.getValue());
    }
}
