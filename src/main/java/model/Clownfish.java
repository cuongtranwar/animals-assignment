package model;

public class Clownfish extends Fish {
    public void sharp() {
        System.out.println("I am small and colourful (orange)");
    }

    public void makeJokes() {
        System.out.println("I make jokes");
    }
}
