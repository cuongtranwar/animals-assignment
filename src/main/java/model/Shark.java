package model;

public class Shark extends Fish {
    public void sharp() {
        System.out.println("I am large and grey");
    }

    public void eat() {
        System.out.println("I eat other fish");
    }
}
