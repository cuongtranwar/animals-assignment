package model;

import constant.AnimalSingEnum;

public class Duck extends Bird {
    @Override
    public void sing(AnimalSingEnum animalSingEnum) {
        System.out.println(animalSingEnum.getValue());
    }

}
