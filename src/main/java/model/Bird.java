package model;

import constant.AnimalSingEnum;

public class Bird {
    public void sing(AnimalSingEnum animalSingEnum) {
        System.out.println(animalSingEnum);
    }

    public void walk() {
        System.out.println("I am walking");
    }

    public void fly() {
        System.out.println("I am flying");
    }

    public void swim() {
        System.out.println("I am swimming");
    }

}

