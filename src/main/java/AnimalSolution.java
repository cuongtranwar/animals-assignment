import abstractfactory.*;
import constant.BirdEnum;
import constant.FishEnum;
import constant.ParrotSingEnum;
import model.*;

public class AnimalSolution {


    public static void main(String[] args) {
        BirdFactory birdFactory = new BirdFactory();
        Chicken chicken = (Chicken) birdFactory.create(BirdEnum.CHICKEN.getValue());
        chicken.fly();
        Parrot parrot = (Parrot) birdFactory.create(BirdEnum.PARROT.getValue());

        parrot.secondvoice(ParrotSingEnum.PARROT_WITH_CAT);
        FishFactory fishFactory = new FishFactory();
        Shark shark = (Shark) fishFactory.create(FishEnum.SHARK.getValue());
        shark.sharp();

        DolphinFactory dolphinFactory = new DolphinFactory();

        Dolphin dolphin = dolphinFactory.create();
        dolphin.swim();


        ButterflyFactory butterflyFactory = new ButterflyFactory();
        Butterfly butterfly = butterflyFactory.create(BirdEnum.BUTTERFLY.getValue());
        butterfly.act();
        Caterpillar caterpillar = (Caterpillar) butterflyFactory.create(BirdEnum.CATERPILLAR.getValue());
        caterpillar.act();

    }
}
