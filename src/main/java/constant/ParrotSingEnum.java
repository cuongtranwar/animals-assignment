package constant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum ParrotSingEnum {

    PARROT_WITH_DOG("Woof, woof"),
    PARROT_WITH_CAT("Meow"),
    PARROT_WITH_ROOSTER("Cock-a-doodle-doo");
    private String value;

    ParrotSingEnum(String value) {
        this.value = value;
    }
}



