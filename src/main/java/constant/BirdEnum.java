package constant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum BirdEnum {
    DUCK("Duck"), CHICKEN("Chicken"), ROOSTER("Rooster"), PARROT("Parrot"), BUTTERFLY("Butterfly"), CATERPILLAR("Caterpillar");
    private String value;


    BirdEnum(String value) {
        this.value = value;
    }
}
