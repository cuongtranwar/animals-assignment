package constant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public enum FishEnum {
    SHARK("Shark"), CLOWN_FISH("Clownfish"), DOLPHIN("Dolphin");
    private String value;


    FishEnum(String value) {
        this.value = value;
    }
}
