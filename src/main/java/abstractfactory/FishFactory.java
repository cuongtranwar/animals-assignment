package abstractfactory;

import constant.FishEnum;
import model.Clownfish;
import model.Fish;
import model.Shark;

public class FishFactory<K extends Fish> implements AnimalFactory<Fish> {
    @Override
    public K create(String animalType) {
        if (animalType.equals(FishEnum.SHARK.getValue())) {
            return (K) new Shark();
        } else if (animalType.equals(FishEnum.CLOWN_FISH.getValue())) {
            return (K) new Clownfish();

        }
        return null;
    }
}
