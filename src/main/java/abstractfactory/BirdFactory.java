package abstractfactory;

import constant.BirdEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import model.*;

public class BirdFactory<K extends Bird> implements AnimalFactory {


    @Override
    public K create(String birdEnum) {

        if (birdEnum.equals(BirdEnum.CHICKEN.getValue())) {
            return (K) new Chicken();
        } else if (birdEnum.equals(BirdEnum.DUCK.getValue())) {
            return (K) new Duck();

        } else if (birdEnum.equals(BirdEnum.ROOSTER.getValue())) {
            return (K) new Duck();

        } else if (birdEnum.equals(BirdEnum.PARROT.getValue())) {
            return (K) new Parrot();
        }
        return null;
    }
}
