package abstractfactory;

public interface AnimalFactory<T> {
    T create(String type);
}