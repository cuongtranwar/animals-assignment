package abstractfactory;

import constant.BirdEnum;
import model.Butterfly;
import model.Caterpillar;

public class ButterflyFactory<K extends Butterfly> implements AnimalFactory {

    @Override
    public K create(String type) {
        if (type.equals(BirdEnum.BUTTERFLY.getValue())) {
            return (K) new Butterfly();
        } else if (type.equals(BirdEnum.CATERPILLAR.getValue())) {
            return (K) new Caterpillar();

        }
        return null;
    }
}
