package abstractfactory;

import model.Dolphin;

public class DolphinFactory {
    public Dolphin create() {
        return new Dolphin();
    }
}
